import { CurrencyExchangeRequest } from "../types/currency-exchange-request.interface";
import { CurrencyExchangeResponse } from "../types/currency-exchange-response.interface";
import Axios from "axios-observable";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { AxiosResponse } from "axios";

export class CurrencyExchangeService {
  // static getCurrencies(
  //   request: CurrenciesRequest
  // ): Observable<{ [key: string]: string }> {
  //   return Axios.get<{ [key: string]: string }>(
  //     "https://openexchangerates.org/api/currencies.json",
  //     {
  //       params: { ...request, app_id: "fede5ce6321e4cc2851beaf5295a4f4f" }
  //     }
  //   ).pipe(switchMap((response: { [key: string]: string }) => of(response)));
  // }

  static getLatestExchangeRates(
    request: CurrencyExchangeRequest
  ): Observable<CurrencyExchangeResponse> {
    return Axios.get<CurrencyExchangeResponse>(
      "https://openexchangerates.org/api/latest.json",
      {
        params: { ...request, app_id: "fede5ce6321e4cc2851beaf5295a4f4f" }
      }
    ).pipe(
      map((response: AxiosResponse<CurrencyExchangeResponse>) => response.data)
    );
  }
}
