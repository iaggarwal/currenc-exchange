export interface CurrencyExchangeRequest {
    base?: string;
    symbols?: Array<string>;
    prettyprint?: boolean;
    show_alternative?: boolean;
}