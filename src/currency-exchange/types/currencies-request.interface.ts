export interface CurrenciesRequest {
    prettyprint?: boolean;
    show_alternative: boolean;
    show_inactive: boolean;
}