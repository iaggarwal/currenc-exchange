import {
  CurrencyExchangeActionTypes,
  CurrencyExchangeActions
} from "./actions";
import { CurrencyExchangeResponse } from "../types/currency-exchange-response.interface";

export interface LatestExchangeState {
  data: CurrencyExchangeResponse;
  error: boolean;
}

export interface CurrenciesState {
  data: { [key: string]: string };
  error: boolean;
}

export interface PocketExchangeState {
  from: String;
  to: String;
}

export interface CurrencyExchangeState {
  latest: LatestExchangeState;
  currencies: CurrenciesState;
  exchangePockets: PocketExchangeState;
}

const initialExchangeState: CurrencyExchangeState = {
  latest: {
    data: {
      disclaimer: "",
      license: "",
      timestamp: -1,
      base: "",
      rates: {}
    },
    error: false
  },
  currencies: {
    data: {},
    error: false
  },
  exchangePockets: {
    from: "USD",
    to: "GBP"
  }
};

export const currencyExchangeReducer = (
  state: CurrencyExchangeState = initialExchangeState,
  action: any
) => {
  switch (action.type) {
    case CurrencyExchangeActionTypes.FETCH_EXCHANGE_RATES_SUCCESS:
      return {
        ...state,
        latest: {
          ...state.latest,
          data: {
            ...state.latest.data,
            ...action.payload
          }
        }
      };
    case CurrencyExchangeActionTypes.FETCH_EXCHANGE_RATES_ERROR:
      return {
        ...state,
        latest: {
          ...state.latest,
          error: true
        }
      };
    case CurrencyExchangeActionTypes.FETCH_CURRENCIES_SUCCESS:
      return {
        ...state,
        currencies: {
          ...state.currencies,
          data: {
            ...state.currencies.data,
            ...action.payload
          }
        }
      };
    case CurrencyExchangeActionTypes.FETCH_CURRENCIES_ERROR:
      return {
        ...state,
        currencies: {
          ...state.currencies,
          error: true
        }
      };
    case CurrencyExchangeActionTypes.SET_FROM_EXCHANGE_POCKET:
      return {
        ...state,
        exchangePockets: {
          ...state.exchangePockets,
          from: action.payload
        }
      };
    case CurrencyExchangeActionTypes.SET_TO_EXCHANGE_POCKET:
      return {
        ...state,
        exchangePockets: {
          ...state.exchangePockets,
          to: action.payload
        }
      };
    default:
      return state;
  }
};
