import { ofType, ActionsObservable } from "redux-observable";
import {
  CurrencyExchangeActionTypes,
  FetchLatestExchangeRatesAction,
  fetchLatestExchangeRatesSuccess,
  FetchLatestExchangeRatesSuccessAction
} from "./actions";
import { map, switchMap } from "rxjs/operators";
import { CurrencyExchangeService } from "../services/Api.ervice";
import { CurrencyExchangeResponse } from "../types/currency-exchange-response.interface";
import { Observable } from "rxjs";

export const fetchLatestCurrenciesEpic = (
  action$: ActionsObservable<FetchLatestExchangeRatesAction>
): Observable<FetchLatestExchangeRatesSuccessAction> =>
  action$.pipe(
    ofType(CurrencyExchangeActionTypes.FETCH_EXCHANGE_RATES),
    switchMap((action: FetchLatestExchangeRatesAction) =>
      CurrencyExchangeService.getLatestExchangeRates(action.payload || {}).pipe(
        map((response: CurrencyExchangeResponse) =>
          fetchLatestExchangeRatesSuccess(response)
        )
      )
    )
  );
