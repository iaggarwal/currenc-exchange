import { CurrencyExchangeRequest } from "../types/currency-exchange-request.interface";
import { CurrenciesRequest } from "../types/currencies-request.interface";
import { CurrencyExchangeResponse } from "../types/currency-exchange-response.interface";

import { Action } from "redux";
import { ErrorResponse } from "../types/error-response.interface";
import { PocketExchangeState } from "./reducer";

export enum CurrencyExchangeActionTypes {
  FETCH_CURRENCIES = "[Revolute] - [Currency Exchange] - Fetch the currencies",
  FETCH_CURRENCIES_SUCCESS = "[Revolute] - [Currency Exchange] - Currencies fetched successfully",
  FETCH_CURRENCIES_ERROR = "[Revolute] - [Currency Exchange] - Error fetching currencies",
  FETCH_EXCHANGE_RATES = "[Revolute] - [Currency Exchange] - Fetch latest exchange rates",
  FETCH_EXCHANGE_RATES_SUCCESS = "[Revolute] - [Currency Exchange] - Latest exchange rates fetched successfully",
  FETCH_EXCHANGE_RATES_ERROR = "[Revolute] - [Currency Exchange] - Error fetching latest exchange rates",
  SET_FROM_EXCHANGE_POCKET = "[Revolute] - [Currency Exchange] - Set from exchange pocket",
  SET_TO_EXCHANGE_POCKET = "[Revolute] - [Currency Exchange] - Set to exchange pocket"
}

export const fetchLatestExchangeRates = (
  payload: CurrencyExchangeRequest
): FetchLatestExchangeRatesAction => ({
  type: CurrencyExchangeActionTypes.FETCH_EXCHANGE_RATES,
  payload
});

export const fetchLatestExchangeRatesSuccess = (
  payload: CurrencyExchangeResponse
): FetchLatestExchangeRatesSuccessAction => ({
  type: CurrencyExchangeActionTypes.FETCH_EXCHANGE_RATES_SUCCESS,
  payload
});

export const fetchLatestExchangeRatesError = (
  payload: ErrorResponse = { error: true }
): FetchLatestExchangeRatesErrorAction => ({
  type: CurrencyExchangeActionTypes.FETCH_EXCHANGE_RATES_ERROR,
  payload
});

export const fetchCurrencies = (
  payload: CurrenciesRequest
): FetchCurrenciesAction => ({
  type: CurrencyExchangeActionTypes.FETCH_CURRENCIES,
  payload
});

export const fetchCurrenciesSuccess = (payload: {
  [key: string]: string;
}): FetchCurrenciesSuccessAction => ({
  type: CurrencyExchangeActionTypes.FETCH_CURRENCIES_SUCCESS,
  payload
});

export const fetchCurrenciesError = (
  payload: ErrorResponse = { error: true }
): FetchCurrenciesErrorAction => ({
  type: CurrencyExchangeActionTypes.FETCH_CURRENCIES_ERROR,
  payload
});

export const setToExchangePockets = (
  payload: string
): SetExchangePocketsAction => ({
  type: CurrencyExchangeActionTypes.SET_TO_EXCHANGE_POCKET,
  payload
});

export const setFromExchangePockets = (
  payload: string
): SetExchangePocketsAction => ({
  type: CurrencyExchangeActionTypes.SET_FROM_EXCHANGE_POCKET,
  payload
});

export interface FetchLatestExchangeRatesAction extends Action {
  type: CurrencyExchangeActionTypes;
  payload?: CurrencyExchangeRequest;
}

export interface FetchLatestExchangeRatesSuccessAction extends Action {
  type: CurrencyExchangeActionTypes;
  payload?: CurrencyExchangeResponse;
}

export interface FetchLatestExchangeRatesErrorAction extends Action {
  type: CurrencyExchangeActionTypes;
  payload?: ErrorResponse;
}

export interface FetchCurrenciesAction extends Action {
  type: CurrencyExchangeActionTypes;
  payload?: CurrenciesRequest;
}

export interface FetchCurrenciesSuccessAction extends Action {
  type: CurrencyExchangeActionTypes;
  payload?: { [key: string]: string };
}

export interface FetchCurrenciesErrorAction extends Action {
  type: CurrencyExchangeActionTypes;
  payload?: ErrorResponse;
}

export interface SetExchangePocketsAction extends Action {
  type: CurrencyExchangeActionTypes;
  payload?: string;
}

export type CurrencyExchangeActions =
  | FetchLatestExchangeRatesAction
  | FetchLatestExchangeRatesSuccessAction
  | FetchLatestExchangeRatesErrorAction
  | FetchCurrenciesAction
  | FetchCurrenciesSuccessAction
  | FetchCurrenciesErrorAction
  | SetExchangePocketsAction;
