import React, { FC, useState } from "react";
import {
  FormControl,
  Select,
  TextField,
  MenuItem,
  InputAdornment,
  FormHelperText
} from "@material-ui/core";
import { PocketState } from "../../account/store/reducer";
import { NumberFormatterService } from "../../services/number-formatter.service";

export interface CurrencyExchangeFieldPropsType {
  currencyOptions: Array<PocketState>;
  selectedCurrency: string;
  onChangeCurrency: Function;
  onChangeAmount: Function;
  exchangeAmount: string;
  placeholder: string;
  icon: Function;
  selectedCurrencyBalance: number;
}

export const CurrencyExchangeField: FC<CurrencyExchangeFieldPropsType> = ({
  currencyOptions,
  selectedCurrency,
  selectedCurrencyBalance,
  onChangeCurrency,
  onChangeAmount,
  exchangeAmount,
  placeholder,
  icon
}) => {
  const renderCurrencyDropdown = (options: Array<PocketState>) =>
    options.map((option: PocketState) => (
      <MenuItem key={option.currency} value={option.currency}>
        {option.currency}
      </MenuItem>
    ));
  const handelAmountOnChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    onChangeAmount(event.target.value);
  };

  const handelCurrencyChange = (
    event: React.ChangeEvent<{ value: unknown }>
  ) => {
    onChangeCurrency(event.target.value as string);
  };

  return (
    <div>
      <FormControl>
        <Select value={selectedCurrency} onChange={handelCurrencyChange}>
          {renderCurrencyDropdown(currencyOptions)}
        </Select>
      </FormControl>
      <TextField
        value={exchangeAmount}
        onChange={handelAmountOnChange}
        placeholder={placeholder}
        type="number"
        InputProps={
          exchangeAmount
            ? {
                startAdornment: (
                  <InputAdornment position="start">{icon()}</InputAdornment>
                )
              }
            : {}
        }
      ></TextField>
      <FormHelperText>{`Balance: ${NumberFormatterService.toCurrency(
        selectedCurrency,
        selectedCurrencyBalance
      )}`}</FormHelperText>
    </div>
  );
};
