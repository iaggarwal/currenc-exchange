import React, { useState, FC } from "react";
import { connect } from "react-redux";
import { RootState } from "../../store";
import {
  CurrencyExchangeField,
  CurrencyExchangeFieldPropsType
} from "../components/CurrencyExchangeField.component";
import { PocketState } from "../../account/store/reducer";
import AddRoundedIcon from "@material-ui/icons/AddRounded";
import RemoveRoundedIcon from "@material-ui/icons/RemoveRounded";
import SwapVertIcon from "@material-ui/icons/SwapVert";
import TrendingUpIcon from "@material-ui/icons/TrendingUp";

import "./CurrencyExchange.component.scss";
import { Fab, Button } from "@material-ui/core";
import { NumberFormatterService } from "../../services/number-formatter.service";
import { updatePockets } from "../../account/store/actions";

export interface CurrencyExchangeUiFormPropsType {
  baseCurrency: string;
  pockets: Array<PocketState>;
  exchangeRates: any;
  updatePockets: any;
}
let CurrencyExchangeUiForm: FC<CurrencyExchangeUiFormPropsType> = ({
  baseCurrency,
  pockets,
  exchangeRates,
  updatePockets
}) => {
  const amountValidationRegEx = /^[1-9]{1}[0-9]*(\.\d{1,2})?$/;
  const [fromPocketAmount, setFromProcketAmount] = useState<string>("");
  const [toPocketAmount, setToProcketAmount] = useState<string>("");
  const [fromCurrency, setFromCurrency] = useState<string>(baseCurrency);
  const initialToCurrency: PocketState =
    (pockets as Array<PocketState>).find(
      (pocket: PocketState) => pocket.currency !== baseCurrency
    ) || (pockets as Array<PocketState>)[1];
  const [toCurrency, setToCurrency] = useState<string>(
    initialToCurrency.currency
  );

  const getExchangeRate = (exchangeRates: any, from: string, to: string) =>
    exchangeRates.data.rates[to] / exchangeRates.data.rates[from];

  const handelFromPocketChange = (amount: string) => {
    if (
      amount === "" ||
      (amountValidationRegEx.test(amount) &&
        parseFloat(amount) <= getBalance(fromCurrency))
    ) {
      setFromProcketAmount(amount);
      setToProcketAmount(
        getConvertedAmount(exchangeRates, amount, fromCurrency, toCurrency)
      );
    }
  };
  const handelToPocketChange = (amount: string) => {
    if (amount === "" || amountValidationRegEx.test(amount)) {
      setToProcketAmount(amount);
      setFromProcketAmount(
        getConvertedAmount(exchangeRates, amount, toCurrency, fromCurrency)
      );
    }
  };

  const handelFromCurrencyChange = (fCur: string) => {
    if (fCur === toCurrency) {
      setToCurrency(fromCurrency);
    }
    setFromCurrency(fCur);
    setToProcketAmount(
      getConvertedAmount(
        exchangeRates,
        fromPocketAmount,
        fCur,
        fCur === toCurrency ? fromCurrency : toCurrency
      )
    );
  };

  const handelToCurrencyChange = (tCur: string) => {
    if (tCur === fromCurrency) {
      setFromCurrency(toCurrency);
    }
    setToCurrency(tCur);
    setFromProcketAmount(
      getConvertedAmount(
        exchangeRates,
        toPocketAmount,
        tCur,
        tCur === fromCurrency ? toCurrency : fromCurrency
      )
    );
  };

  const handelSwap = () => {
    setFromCurrency(toCurrency);
    setToCurrency(fromCurrency);
    setFromProcketAmount(toPocketAmount);
    setToProcketAmount(fromPocketAmount);
  };

  const handelFormSubmit = (event: React.FormEvent) => {
    event.preventDefault();
    const updatedPocket = pockets.reduce((acc, pocket) => {
      if (pocket.currency === fromCurrency) {
        return [
          ...acc,
          { ...pocket, balance: pocket.balance - parseFloat(fromPocketAmount) }
        ];
      } else if (pocket.currency === toCurrency) {
        return [
          ...acc,
          { ...pocket, balance: pocket.balance + parseFloat(toPocketAmount) }
        ];
      }
      return acc;
    }, [] as Array<PocketState>);
    updatePockets(updatedPocket);
  };

  const getFromIcon = () => <RemoveRoundedIcon fontSize="small"/>;
  const getToIcon = () => <AddRoundedIcon fontSize="small" />;

  const getBalance = (currency: string) =>
    (
      pockets.find(pocket => pocket.currency === currency) ||
      ({} as PocketState)
    ).balance || 0;

  const getConvertedAmount = (
    exchangeRates: any,
    amount: string,
    from: string,
    to: string
  ) => {
    return !!amount
      ? Math.round(
          parseFloat(amount) * getExchangeRate(exchangeRates, from, to) * 100
        ) /
          100 +
          ""
      : "";
  };

  const getCurrencyExchangeField = (
    currencyExchangeFieldProps: CurrencyExchangeFieldPropsType
  ): JSX.Element => <CurrencyExchangeField {...currencyExchangeFieldProps} />;

  return (
    <form
      className="CurrencyExchangeForm"
      noValidate
      autoComplete="off"
      onSubmit={handelFormSubmit}
    >
      <section className="CurrencyExchangeFormBody">
        <div className="CurrencyExchangeFormBodyFieldFrom">
          {getCurrencyExchangeField({
            currencyOptions: pockets,
            selectedCurrency: fromCurrency,
            onChangeCurrency: handelFromCurrencyChange,
            onChangeAmount: handelFromPocketChange,
            exchangeAmount: fromPocketAmount,
            placeholder: "0",
            icon: getFromIcon,
            selectedCurrencyBalance: getBalance(fromCurrency)
          })}
        </div>
        <div className="Icons">
          <div className="Swap" onClick={handelSwap}>
            <SwapVertIcon className="ExchangeIcon" fontSize="small" />
          </div>
          <div className="ExchangeRate">
            <TrendingUpIcon className="ExchangeIcon" fontSize="small" />
            <div>{NumberFormatterService.toCurrency(fromCurrency, 1)}</div>
            <span>=</span>
            <div>
              {NumberFormatterService.toCurrency(
                toCurrency,
                parseFloat(
                  getConvertedAmount(
                    exchangeRates,
                    "1",
                    fromCurrency,
                    toCurrency
                  )
                )
              )}
            </div>
          </div>
        </div>
        <div className="CurrencyExchangeFormBodyFieldTo">
          {getCurrencyExchangeField({
            currencyOptions: pockets,
            selectedCurrency: toCurrency,
            onChangeCurrency: handelToCurrencyChange,
            onChangeAmount: handelToPocketChange,
            exchangeAmount: toPocketAmount,
            placeholder: "0",
            icon: getToIcon,
            selectedCurrencyBalance: getBalance(toCurrency)
          })}
        </div>
      </section>
      <section className="CurrencyExchangeFormFooter">
        <Button
          type="submit"
          disabled={
            !fromPocketAmount ||
            fromPocketAmount === "0" ||
            getBalance(fromCurrency) <= 0
          }
        >
          Exchange
        </Button>
      </section>
    </form>
  );
};

const mapStateToProps = (state: RootState) => ({
  pockets: state.account.data.pockets,
  baseCurrency: state.account.data.baseCurrency,
  exchangeRates: state.currencyExchange.latest
});

const mapPropsToDispatch = {
  updatePockets
};

export default connect(
  mapStateToProps,
  mapPropsToDispatch
)(CurrencyExchangeUiForm);
