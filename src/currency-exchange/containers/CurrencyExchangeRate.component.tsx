import React, { FC } from "react";
import { connect } from "react-redux";
import { RootState } from "../../store";

const CurrencyExchangeRate: FC<any> = props => {
  const { fromCurrency, toCurrency, exchangeRates } = props;

  const getExchangeRate = (exchangeRates: { [key: string]: number }) =>
    Object.keys(exchangeRates)
      .reduce(
        (acc, currency) => {
          if (currency === fromCurrency) {
            acc[0] = exchangeRates[currency];
          }

          if (currency === toCurrency) {
            acc[1] = exchangeRates[currency];
          }

          return acc;
        },
        [0, 0]
      )
      .map(x => x || 1)
      .reduce((acc, val) => val / acc, 1);

  return (
    <div>
      <div>
        <span>1</span>
        <span>{fromCurrency}</span>
      </div>

      <div>
        <span>{getExchangeRate(exchangeRates)}</span>
        <span>{toCurrency}</span>
      </div>
    </div>
  );
};

const mapStateToProps = (state: RootState) => {
    
}

export default connect()(CurrencyExchangeRate);
