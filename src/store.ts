import { combineReducers, applyMiddleware, createStore } from "redux";
import { combineEpics, createEpicMiddleware } from "redux-observable";
import { currencyExchangeReducer, CurrencyExchangeState } from "./currency-exchange/store/reducer";
import { fetchLatestCurrenciesEpic } from "./currency-exchange/store/epic";
import { fetchAccountEpic, updatePocketsEpic } from "./account/store/epics";
import { accountReducer, AccountState } from "./account/store/reducer";
import { composeWithDevTools } from "redux-devtools-extension";

export interface RootState {
  currencyExchange: CurrencyExchangeState,
  account: AccountState
}

const rootReducer = combineReducers({
  currencyExchange: currencyExchangeReducer,
  account: accountReducer
});

const rootEpic = combineEpics<any>(
  fetchLatestCurrenciesEpic,
  fetchAccountEpic,
  updatePocketsEpic
);

const epicMiddleware = createEpicMiddleware();

export const store = createStore(
  rootReducer,
  composeWithDevTools(applyMiddleware(epicMiddleware))
);

epicMiddleware.run(rootEpic);
