import { AccountRequest } from "../types/account-request.interface";
import { AccountInfoState, PocketState } from "../store/reducer";
import { Observable, of, BehaviorSubject } from "rxjs";
import { mapTo, delay, map } from "rxjs/operators";

export class AccountApiService {
  static account$ = new BehaviorSubject({
    name: "Ishan Aggarwal",
    baseCurrency: "USD",
    pockets: [
      { currency: "USD", balance: 1000 },
      { currency: "GBP", balance: 200 },
      { currency: "EUR", balance: 50 }
    ]
  });
  static fetchAccount(request: AccountRequest): Observable<AccountInfoState> {
    return AccountApiService.account$.pipe(delay(500));
  }

  static updateAccountPockets(pockets: Array<PocketState>): Observable<AccountInfoState> {
    return AccountApiService.account$.pipe(
      map(account => {
        return {
          ...account,
          pockets: account.pockets.map(pocket => {
            const updated = pockets.find(
              p => pocket.currency === pocket.currency
            );
            return { ...pocket, ...(updated || {}) };
          })
        };
      })
    );
  }
}
