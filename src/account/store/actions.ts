import { Action } from "redux";
import { PocketState, AccountInfoState } from "./reducer";
import { ErrorResponse } from "../../currency-exchange/types/error-response.interface";
import { AccountRequest } from "../types/account-request.interface";

export enum AccountActionTypes {
  FETCH_ACCOUNT = "[Revolute] - [ACCOUNT] - Fetch the account information",
  FETCH_ACCOUNT_SUCCESS = "[Revolute] - [ACCOUNT] - account information fetched successfully",
  FETCH_ACCOUNT_ERROR = "[Revolute] - [ACCOUNT] - Error fetching account information",
  UPDATE_POCKETS = "[Revolute] - [ACCOUNT] - Update pockets",
  UPDATE_POCKETS_SUCCESS = "[Revolute] - [ACCOUNT] - Pockets updated successfully",
  UPDATE_POCKETS_ERROR = "[Revolute] - [ACCOUNT] - Error updating the pockets"
}

export interface FetchAccountActionType extends Action {
  type: AccountActionTypes;
  payload?: AccountRequest;
}

export interface FetchAccountSuccessActionType extends Action {
  type: AccountActionTypes;
  payload?: AccountInfoState;
}

export interface FetchAccountErrorActionType extends Action {
  type: AccountActionTypes;
  payload?: ErrorResponse;
}

export interface UpdatePocketsActionType extends Action {
  type: AccountActionTypes;
  payload?: Array<PocketState>;
}

export interface UpdatePocketsSuccessActionType extends Action {
  type: AccountActionTypes;
  payload?: Array<PocketState>;
}

export interface UpdatePocketsErrorActionType extends Action {
  type: AccountActionTypes;
  payload?: ErrorResponse;
}

export const fetchAccountInformation = (
  payload: AccountRequest
): FetchAccountActionType => ({
  type: AccountActionTypes.FETCH_ACCOUNT,
  payload
});

export const fetchAccountInformationSuccess = (
  payload: AccountInfoState
): FetchAccountSuccessActionType => ({
  type: AccountActionTypes.FETCH_ACCOUNT_SUCCESS,
  payload
});

export const fetchAccountInformationError = (
  payload: ErrorResponse
): FetchAccountErrorActionType => ({
  type: AccountActionTypes.FETCH_ACCOUNT_ERROR,
  payload
});

export const updatePockets = (
  payload: Array<PocketState>
): UpdatePocketsActionType => ({
  type: AccountActionTypes.UPDATE_POCKETS,
  payload
});

export const updatePocketsSuccess = (
  payload: Array<PocketState>
): UpdatePocketsSuccessActionType => ({
  type: AccountActionTypes.UPDATE_POCKETS_SUCCESS,
  payload
});

export const updatePocketsError = (
  payload: ErrorResponse
): UpdatePocketsErrorActionType => ({
  type: AccountActionTypes.UPDATE_POCKETS_ERROR,
  payload
});

export type AccountActions =
  | FetchAccountActionType
  | FetchAccountSuccessActionType
  | FetchAccountErrorActionType
  | UpdatePocketsActionType
  | UpdatePocketsSuccessActionType
  | UpdatePocketsErrorActionType;
