import { AccountActions, AccountActionTypes } from "./actions";
import { ErrorResponse } from "../../currency-exchange/types/error-response.interface";

export interface PocketState {
  currency: string;
  balance: number;
}

export interface AccountInfoState {
  name: string;
  baseCurrency: string;
  pockets: Array<PocketState>;
}

export interface AccountState {
  data: AccountInfoState;
  failure: ErrorResponse;
}

const initialAccountState: AccountState = {
  data: {
    name: "",
    baseCurrency: "USD",
    pockets: [
      {
        currency: "USD",
        balance: 0
      },
      {
        currency: "GBP",
        balance: 0
      },
      {
        currency: "EUR",
        balance: 0
      }
    ]
  },
  failure: {
    error: false,
    messgae: ""
  }
};

export const accountReducer = (
  state: AccountState = initialAccountState,
  action: AccountActions
) => {
  switch (action.type) {
    case AccountActionTypes.FETCH_ACCOUNT_SUCCESS:
      return {
        ...state,
        data: {
          ...state.data,
          ...action.payload
        }
      };
    case AccountActionTypes.FETCH_ACCOUNT_ERROR ||
      AccountActionTypes.UPDATE_POCKETS_ERROR:
      return {
        ...state,
        failure: {
          error: true
        }
      };
    case AccountActionTypes.UPDATE_POCKETS_SUCCESS:
      return {
        ...state,
        data: {
          ...state.data,
          pockets: state.data.pockets.map(pocket => {
            const updatePocket = (action.payload as Array<PocketState>).filter(
              p => p.currency === pocket.currency
            )[0];
            return {
              ...pocket,
              ...updatePocket
            };
          })
        },
        failure: {
          error: false,
          message: ""
        }
      };
    default:
      return state;
  }
};
