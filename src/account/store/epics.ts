import { ofType, ActionsObservable } from "redux-observable";
import {
  FetchAccountSuccessActionType,
  FetchAccountActionType,
  AccountActionTypes,
  fetchAccountInformationSuccess,
  UpdatePocketsActionType,
  UpdatePocketsSuccessActionType,
  updatePocketsSuccess
} from "./actions";
import { map, switchMap } from "rxjs/operators";
import { Observable } from "rxjs";
import { AccountApiService } from "../services/account.api.service";
import { AccountInfoState } from "./reducer";

export const fetchAccountEpic = (
  action$: ActionsObservable<FetchAccountActionType>
): Observable<FetchAccountSuccessActionType> =>
  action$.pipe(
    ofType(AccountActionTypes.FETCH_ACCOUNT),
    switchMap((action: FetchAccountActionType) =>
      AccountApiService.fetchAccount({
        userId: "ishan.aggarwal13@gmail.com"
      }).pipe(
        map((response: AccountInfoState) =>
          fetchAccountInformationSuccess(response)
        )
      )
    )
  );

export const updatePocketsEpic = (
  action$: ActionsObservable<UpdatePocketsActionType>
): Observable<UpdatePocketsSuccessActionType> =>
  action$.pipe(
    ofType(AccountActionTypes.UPDATE_POCKETS),
    switchMap((action: UpdatePocketsActionType) =>
      AccountApiService.updateAccountPockets(action.payload || []).pipe(
        map(
          (response: AccountInfoState): UpdatePocketsSuccessActionType =>
            updatePocketsSuccess(action.payload || [])
        )
      )
    )
  );
