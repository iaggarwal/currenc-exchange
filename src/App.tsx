import React from "react";
import "./App.scss";
import { Provider } from "react-redux";
import { store } from "./store";
import { fetchLatestExchangeRates } from "./currency-exchange/store/actions";
import { fetchAccountInformation } from "./account/store/actions";
import CurrencyExchangeUiForm from "./currency-exchange/containers/CurrencyExchange.component";

const App: React.FC = () => {
  store.dispatch(
    fetchAccountInformation({ userId: "ishan.aggarwal13@gmail.com" })
  );
  store.dispatch(fetchLatestExchangeRates({}));
  return (
    <Provider store={store}>
      <div className="App">
        <nav className="Navigation"></nav>
        <main className="Main">
          <CurrencyExchangeUiForm />
        </main>
      </div>
    </Provider>
  );
};

export default App;
