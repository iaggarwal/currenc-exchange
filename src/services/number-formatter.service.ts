export class NumberFormatterService {
  static roundTo = (maximumFractionDigits: number, num: string) =>
    new Intl.NumberFormat("en", { maximumFractionDigits: 2 }).format(
      parseFloat(num)
    );

  static toCurrency = (currency: string, amount: number) =>
    new Intl.NumberFormat("en", { style: "currency", currency }).format(
      amount
    );
}
